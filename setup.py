#!/usr/bin/env python
# coding: utf-8
from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

import os,sys

include_dirs = os.popen("pkg-config pkg-config --cflags-only-I lqr-1").read()
if not include_dirs.strip():
    sys.exit(1)
include_dirs = [dir_.lstrip("-I") for dir_ in   include_dirs.split()]



setup(
    cmdclass = {'build_ext': build_ext},
    ext_modules = [Extension("lqr", ["lqr.pyx"],
        libraries=["lqr-1"],
        include_dirs=include_dirs)],
    requires=["PIL (>=1.4)", "Cython"],
    provides=["pylqr", "liquid_resize", "lqr"]
)
    

