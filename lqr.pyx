# coding: utf-8

from libc.stdlib cimport malloc, free
from libc.string cimport memcpy
cdef extern from *:
    ctypedef char const_char "const char"
    ctypedef void const_void "const void"

cdef extern:
    cdef void image_reader(void *buffer, LqrCarver * Carver)
    
#cdef extern from "string.h" nogil:

#    void *memcpy  (void *TO, unsigned char *FROM, int SIZE)

cdef extern from "lqr-1/lqr.h":
    ctypedef struct LqrCarver:
        pass

    LqrCarver * lqr_carver_new(unsigned char * buffer,
        int width,
        int height,
        int channels)

    void lqr_carver_destroy(LqrCarver *carver)

    LqrRetVal lqr_carver_init(LqrCarver *carver,
            int delta_x,
            float rigidity)

    LqrRetVal lqr_carver_set_image_type(LqrCarver *carver,
         LqrImageType image_type)

    LqrRetVal lqr_carver_resize(LqrCarver *carver,
         int new_width,
         int new_height)

    int lqr_carver_scan_by_row(LqrCarver *carver)

    int lqr_carver_scan_line_ext(LqrCarver *carver,
            int* n,
            void** rgb)

    int lqr_carver_flatten(LqrCarver *carver)
    void lqr_carver_scan_reset(LqrCarver *carver)

cdef enum LqrRetVal:
    LQR_ERROR, LQR_OK, LQR_OUT_OF_MEMORY, LQR_CANCELLED

cdef enum LqrImageType:
    GREY_IMAGE, GREYA_IMAGE, RGB_IMAGE, RGBA_IMAGE, CMY_IMAGE, \
            CMYK_IMAGE, CMYKA_IMAGE, CUSTOM_IMAGE

# Python stdlib imports:
import array

# Python third party imports:
from PIL import Image

channel_names = {"RGB": 3, "RGBA": 4}

image_type_names = {"GREY": GREY_IMAGE, "GREYA": GREYA_IMAGE,
    "RGB": RGB_IMAGE, "RGBA": RGBA_IMAGE, "CMY": CMY_IMAGE, 
    "CMYK": CMYK_IMAGE, "CMYKA": CMYKA_IMAGE, "CUSTOM": CUSTOM_IMAGE }


def check_retval(retval):
    if retval != LQR_OK:
        if retval == LQR_OUT_OF_MEMORY:
            raise MemoryError
        elif retval == LQR_CANCELLED:
            raise KeyboardInterrupt
        raise Exception("Unknown errror in liblqr")

cdef class Carver:
    """
    Call with Carver(PIL.Image image, delta_x=1, rigidity=)
    to create a carver object which can be resizes using
    the Liquid Rescale algorithm
    """
    cdef LqrCarver * carver
    cdef object __mode
    cdef readonly int channels
    cdef readonly object new_size
    cdef unsigned char * data
    cdef object data_holder

    def __cinit__(self, img, delta_x=1, rigidity=0):
        # TODO: Accept initialization without an image
        # and add ways to be work independet of PIL
        w, h = img.size
        self.channels = channels = channel_names[img.mode]
        byte_size = w * h * channels
        tmp = img.tostring()
        cdef void * buffer_ = malloc(byte_size)
        if not buffer_:
            raise MemoryError
        self.data = <unsigned char *>buffer_
        memcpy(self.data, <const_void *>tmp, byte_size)
        self.carver = lqr_carver_new(self.data, w, h, channels)
        # The carver objects takes care of freeing
        # the buffer when de-allocated.
        check_retval(lqr_carver_init(self.carver, delta_x, rigidity))
        self.__mode = img.mode

    def resize_internal(self, size):
        new_width, new_height = size
        check_retval(lqr_carver_resize(self.carver, new_width, new_height))
        self.new_size = size
        #lqr_carver_flatten(self.carver)

    def read_image(self):
        """
        This returns the image data after an internal_resize is made.
        It returns (direction, image_data)  the first is a boolean,
        which if False, indicates the image is transposed. The second is
        a python array.array object
        """

        byte_size = self.new_size[0] * self.new_size[1] * self.channels
        image_data = array.array("B", "\x00" * byte_size)

        # "The function returns TRUE if the image is read by
        # row, and FALSE if it is read by column.":
        direction = lqr_carver_scan_by_row(self.carver)
        cdef int line_size = (self.new_size[0] if direction else
            self.new_size[1]) * self.channels

        cdef void *line_data = <void *>0

        lqr_carver_scan_reset(self.carver)
        cdef long int tmp
        cdef int line = 0
        cdef long int base_address = image_data.buffer_info()[0]
        cdef void * line_address
        # The scan_line bellow allocates a line buffer by itself
        # a reference to which is kept in the Carver struct
        while lqr_carver_scan_line_ext(self.carver, &line, &line_data):
            tmp = base_address + line * line_size
            line_address = <void *>tmp
            memcpy(line_address, line_data, line_size)

            #image_data[line_size * line: (line_size * line + 1)] = line_data

        return direction, image_data

    def __dealloc__(self):
        lqr_carver_destroy(self.carver)


    def resize(self, size):
        """
        Pil.Image img = Carver.resize((width, height))
        Returns a PIL Image with the resized image
        using current settings; - The main method for quick usage.
        """
        self.resize_internal(size)
        direction, image_data = self.read_image()
        size = self.new_size if direction else \
            (self.new_size[1], self.new_size[0])
        resized_img =  Image.frombuffer(self.__mode, size,
            buffer(image_data), "raw", self.__mode, 0, 1)
        if not direction:
            resized_img = resized_img.transpose(Image.ROTATE_270
                ).transpose(Image.FLIP_LEFT_RIGHT)
        return resized_img

    def _set_image_mode(self, mode):
        # Apparently one can't set the mode back to RGB if it is set to CMY
        check_retval(lqr_carver_set_image_type(self.carver,
            image_type_names[mode]))
        self.__mode = mode

    def _get_image_mode(self):
        return self.__mode
    
    mode = property(_get_image_mode, _set_image_mode)
    #TODO: Support for other image formats
    # Support for adding BIAS and rigitdit masks
    
    """LqrRetVal lqr_carver_set_alpha_channel(      LqrCarver * carver,
         gint channel_index);

        LqrRetVal lqr_carver_set_black_channel(  LqrCarver * carver,
         gint channel_index);
    """


__all__ = ["Carver"]